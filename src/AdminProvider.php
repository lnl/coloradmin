<?php

namespace LocknLoad\Admin;

use Illuminate\Support\ServiceProvider;

class AdminProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        include __DIR__.'/Macros.php';
        include __DIR__.'/Comands.php';

        $this->loadViewsFrom(__DIR__.'/views', 'admin');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {

        $this->commands([
            'LocknLoad\Admin\Comands'
        ]);

        $this->app->singleton('Macros', function () {
            return new Macros();
        });

     }
}
