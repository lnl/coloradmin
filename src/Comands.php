<?php

namespace LocknLoad\Admin;

use Illuminate\Console\Command;

class Comands extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'locknload-admin:assets';

    /**
     * The console command description.
     *ra
     * @var string
     */
    protected $description = 'Minifica e gera o uglify dos assets do tema admin';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $version = env('ASSETS_VERSION');

        echo "\r\nLimpando o diretorio public";
        exec('rm public/assets/js/*.js');
        exec('rm public/assets/css/*.css');
        exec('rm public/assets/fonts/*.*');

        echo "\r\nuglify e unindo arquivos js";
        $dirs = array_filter(glob(__DIR__.'/assets/js/*'), 'is_dir');

        foreach ($dirs as $dir){
            $name = explode("/",$dir)[sizeof(explode("/",$dir))-1];
            exec('uglifyjs '.__DIR__.'/assets/js/'.$name.'/*.js -c -o '.public_path().'/assets/js/'.$name.'-'.$version.'.min.js');
        }

        echo "\r\nunindo e minificando arquivos css";
        $dirs = array_filter(glob(__DIR__.'/assets/css/*'), 'is_dir');

        foreach ($dirs as $dir){
            $name = explode("/",$dir)[sizeof(explode("/",$dir))-1];
            exec('node-sass '.__DIR__.'/assets/css/'.$name.'/main.scss > '.public_path().'/assets/css/'.$name.'-'.$version.'.min.css');
        }
        echo "\r\ncopiando arquivos de fonts";
        exec('cp '.__DIR__.'/assets/fonts/*.* '.public_path().'/assets/fonts/');

    }
}
