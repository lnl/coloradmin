<div class="modal modal-message fade" id="banco-imagens">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">BANCO DE IMAGENS</h4>
            </div>
            <div class="modal-body" style="height: 500px; width:90%; display: block;   overflow: scroll">
            </div>
        </div>
    </div>
</div>

