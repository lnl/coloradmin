@extends('admin::base.container')

<?php
use \LocknLoad\Crud\Helper;
use \Locknload\Admin\Macros;
use \LocknLoad\Crud\Fields;

$model = Helper::generateModel( $data['class'] );
?>

@section('conteudo')

    <div id="content" class="content">

        <h1 class="page-header"> {{Helper::translateField( $data['class'] )}} <small>  Lista de itens </small></h1>

         <div class="row">

            <div class="col-md-12">

                <div class="panel panel-inverse">

                    <div class="panel-heading">
                        <h4 class="panel-title">Listar {{Helper::translateField( $data['class'] )}}</h4>
                    </div>

                    <div class="panel-body">
                        <div class="table-responsive">
                            <a href="/editar/{{$data['class']}}" class="btn btn-success btn-block">Add {{Helper::translateField( $data['class'] )}} </a>
                        </div>

                        <div class="table-responsive">

                            @if($model::getSearchField())
                                <form action="{{Request::url()}}" method="get">
                                    Filtre pelo campo {{ $model::getSearchField()}} : <input type="text" name="{{$model::getSearchField()}}" value="">
                                    <input type="submit" value="filtrar">
                                </form>

                                <br/>
                            @endif

                            @if (sizeof($data['objs']) > 0 && sizeof($data['columns']) > 0)

                                <table id="data-table" class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            @foreach ($data['columns'] as $col)
                                                <th>{{ Helper::translateField($col['field']) }}</th>
                                            @endforeach
                                            <th>Ações</th>
                                        </tr>
                                    </thead>

                                    <tbody>

                                        @foreach ($data['objs'] as $row)

                                            <tr class="odd gradeX">

                                                @foreach ($data['columns'] as $col)

                                                    <td>

                                                        @if ( Fields::isRelation($col) )
                                                           {!! Macros::showListRelation($row, $col) !!}
                                                        @elseif ( Fields::isColor($col) )
                                                           {!! Macros::showListColor($row, $col) !!}
                                                        @elseif ( Fields::isImage($col) )
                                                           {!! Macros::showListImage($row, $col, array("width" => "100px")) !!}
                                                        @elseif ( Fields::isBool($col) )
                                                           {!! Macros::showListBool($row, $col) !!}
                                                        @else
                                                           {!! Macros::showListText($row, $col) !!}
                                                        @endif

                                                    </td>

                                                @endforeach

                                                <td>
                                                        <a class="btn btn-success" href="/editar/{{$data['class']}}/{{ isset($row['id'])? $row['id'] : $row['hash'] }}" ><i class="fa fa-edit"></i></a>

                                                    @if(sizeof($row->getExtraActions()) > 0)
                                                        @foreach( $row->getExtraActions() as $title => $button)
							    @if($button['level'] <= Auth::user()->level())
								@if(isset($button['id']))
								<a class="btn btn-success" title="{{$title}}" href="{{$button['link']}}{{$row[$button['id']]}}" ><i class="fa {{$button['icon']}}"></i></a>
	@else
								<a class="btn btn-success" title="{{$title}}" href="{{$button['link']}}{{$row['id']}}" ><i class="fa {{$button['icon']}}"></i></a>
								@endif
                                                            @endif
                                                        @endforeach
                                                    @endif

                                                    <a class="btn btn-danger"  href="{{ $row['id'] }}"><i class="fa fa-trash-o "></i></a>
                                                    @if($data['class'] == 'ed_materia')
                                                        @if( Session::has('fb_access_token') )
                                                            <a class="btn btn-primary" href="/social/materia/{{$row['id']}}" ><i class="fa fa-facebook"></i></a>
                                                        @endif
                                                     @endif
                                                </td>

                                            </tr>

                                        @endforeach

                                    </tbody>

                                </table>

			       {{$data['objs']->appends($data['qs'])->links()}}

                            @else

                                <p> Você nao possui conteudos para {{Helper::translateField( $data['class'] )}} cadastrados.... </p>

                            @endif

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

@stop

@section('script')
    <script>

        $(".btn.btn-danger").click(function(e){
            e.preventDefault();

            var obj = $(e.target).prop("tagName") == "A" ? $(e.target) : $(e.target).parents("a");
            var value = obj.attr("href");

            $.ajax({
                url:  "/{{$data['class']}}/",
                type: 'DELETE',
                data: { id: value , _token: "{{ csrf_token() }}" },
                success: function(result) {
                    location.reload();
                }

            });

        });

        var mkt =  function(e){
          var form = $(e).parents("form");

          form.attr('action', form.attr('action').replace('listar','mkt'));
          form.submit();
        }

    </script>
@stop
