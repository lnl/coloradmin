<?php

use \locknload\Admin\Macros;
use \locknload\Crud\Helper;
use \locknload\Crud\Fields;

?>

{{ Form::open(['url' => '/api/'.$data['class'], 'method' => 'POST', 'enctype' => 'multipart/form-data', 'class' => 'form-horizontal form-bordered ajax', 'role' => 'form']) }}

    <div class="row">
        @if($data['struct'])

            @foreach ($data['columns'] as $container)

                    <input class="form-control" type="hidden" value="{{$data['obj']['id']}}" name="id" />

                    @foreach ($container as $fieldk => $field)

                        @if ($fieldk != 'container_header')
                        <div class="col-md-<?= $field['form_size'] == 'big' ? "12" : "6"?>" style="margin:15px 0;">
                            <label class="control-label col-md-4">
                                {!!Macros::generateLabel(isset($field['label'])? $field['label']:$field['field'],$field['nullable'])!!}
                            </label>

                            <div class="col-md-8">
                                @if ( $field['type'] == 'multiselect' )
                                    {!!Macros::getMultiSelect($field, $data)!!}
                                @elseif ( $field['type'] == 'select' )
                                    {!!Macros::getSelect($field, $data)!!}
                                @elseif ( $field['type'] == 'date' )
                                    {!!Macros::getDate($field, $data)!!}
                                @elseif ( $field['type'] == 'image' )
                                    {!!Macros::getFile($field)!!}
                                @elseif ( $field['type'] == 'color' )
                                    {!!Macros::getColor($field, $data)!!}
                                @elseif ( $field['type'] == 'bool' )
                                    {!!Macros::getCheckBox($field, $data, array("data-render"=>"switchery"))!!}
                                @elseif ( $field['type'] == 'text' )
                                    {!!Macros::getTextArea($field, $data, array("style" => "width:100%"))!!}
                                @else
                                    {!!Macros::getText($field, $data)!!}
                                @endif
                            </div>
                        </div>
                        @endif

                    @endforeach

                </div>
            @endforeach

        @endif
    </div>

{{ Form::close() }}
