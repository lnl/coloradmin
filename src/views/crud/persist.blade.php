@extends('admin::base.container')

<?php

use LocknLoad\Crud\Helper;

?>

@section('script')
    <script>
        FormSliderSwitcher.init();
    </script>

    <script>


        $("form .btn.btn-remove").click(function(e){
            e.preventDefault();

            var obj = $(e.target).prop("tagName") == "A" ? $(e.target) : $(e.target).parents("a");
            var form = obj.parents("form");

            $.ajax({
                url:  form.attr("action"),
                type: 'DELETE',
                data: form.serialize(),
                success: function(result) {
                    location.reload();
                }

            });

        });

        $(".filters-content").submit(function(e){

            e.preventDefault();

            var obj = $(e.target);
            var htmlFilter = obj.parents(".filter-item").clone();

            $.ajax({
                method: obj.attr('method'),
                url: obj.attr('action'),
                data: obj.serialize()
            })
            .done(function( msg ) {
                console.log( "Data Saved: " + msg  );

                obj.parents(".filters-content").append(htmlFilter).html();

                loadBinds();

            });

        })


        /*funcao especifica do form de filtros*/

        var loadBinds = function(){
            $("select[name=relationValue]").unbind("change");
            $("select[name=relationValue]").change(function(e){

                var obj = $(e.target);
                var form = obj.parent("form");

                $.get("/api/listar/ec_produto/gb_filtro/"+form.find("select[name=relationValue]").val(), function(data){

                    var html = "<option value=\"\">Valores existentes</option>";

                    for (var i=0; i < data.length; i++){
                        for (var x=0; x < data[i].filtro.length; x++){

                            html += "<option valor=\""+data[i].filtro[x].pivot.valor+"\">"+data[i].filtro[x].pivot.valor+"</option>";

                        }
                    }

                    obj.parent().find("select[name=extraFieldValue]").html(html);
                    obj.parent().find("select[name=extraFieldValue]").prop('disabled',false);

                });

            });

        }();

        @if (isset($data['liveeditor']) && $data['liveeditor'])
            $(document).ready(function() {
                $(".sidebar-minify-btn").click();
            });
        @endif

    </script>
@stop

@section('conteudo')

<style>
span.select2-container--default .select2-selection--multiple {
height: auto !important;
}
</style>

    <div id="content" class="content">

        <h1 class="page-header"> Editar {!!Helper::translateField($data['class'])!!} </h1>

        {{ Form::open(['url' => '/'.$data['class'], 'method' => 'POST', 'enctype' => 'multipart/form-data', 'class' => 'form-horizontal form-bordered', 'role' => 'form']) }}

            <div class="row">
                @if($data['struct'])
                    @include('admin::crud.formStruct', ['data' => $data])
                @else
                    @include('admin::crud.form', ['data' => $data])
                @endif
            </div>

        {{ Form::close() }}

        @if (isset($data['dropzone']) && $data['dropzone'])
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-inverse" data-sortable-id="form-stuff-2">
                        <div class="panel-heading">
                            <h4 class="panel-title">Imagens para detalhe de produto</h4>
                        </div>

                        <div class="panel-body">
                            <ul style="margin: 0 0 5px 0;padding: 0;list-style: none;">
                                @forelse($data['obj']->images as $i)
                                <li style="display:inline-block; margin-right:5px;"><img src="{{$i->imagem}}" width="150px"></li>
                                @empty
                                @endforelse
                            </ul>

                            <div id="dropzone">
                              {{ Form::open(['url' => '/gb_image/'.$data['class'].'/'.$data['obj']->id, 'method' => 'POST', 'class' => 'dropzone needsclick', 'id'=>'image-drop']) }}
                                <div class="dz-message needsclick">
                                  Drop files <b>here</b> or <b>click</b> to upload.<br />
                                </div>
                              {{Form::close()}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif

        @if (isset($data['combination']) && $data['combination'])
            <div class="row">

                <div class="col-md-12">

                    <div class="panel panel-inverse" data-sortable-id="form-stuff-2">

                        <div class="panel-heading">

                            <div class="panel-heading-btn">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                            </div>

                            <h4 class="panel-title">Grupo de informações</h4>

                        </div>

                        <div class="panel-body filters-content">
                            @include('admin.combination', ['filters' => $data['combination'],'obj' => $data['obj']])
                        </div>

                    </div>

                </div>

            </div>

        @endif

        @if (isset($data['filters']) && $data['filters'])
            <div class="row">

                <div class="col-md-12">

                    <div class="panel panel-inverse" data-sortable-id="form-stuff-2">

                        <div class="panel-heading">

                            <div class="panel-heading-btn">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                            </div>

                            <h4 class="panel-title">Gestor de filtros</h4>

                        </div>

                        <div class="panel-body filters-content">
                            @include('admin.filters', ['filters' => $data['filters'],'obj' => $data['obj']])
                        </div>

                    </div>

                </div>

            </div>

        @endif

    </div>



@stop
