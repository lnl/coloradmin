<?php use Illuminate\Support\Facades\Auth ?>
<!DOCTYPE html>
<!--[if IE 8]>
  <html lang="br" class="ie8"> <![endif]-->
<!--[if !IE]><!-->
  <html lang="br">
<!--<![endif]-->

  <head>
    <meta charset="utf-8" />
    <title>{{env('APP_NAME')}} Admin | Login Page</title>
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />

    <!-- ================== BEGIN BASE CSS STYLE ================== -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
    <link href="{{ URL::asset('/assets/css/core-'.env('ASSETS_VERSION').'.min.css')}}" rel="stylesheet" id="theme" />
    <link href="{{ URL::asset('/assets/css/form-'.env('ASSETS_VERSION').'.min.css')}}" rel="stylesheet" id="theme" />

    <style>
        body {
            height: 100%;
            position: absolute;
            width: 100%;
        }

        #content, #page-container{
            height:100%;
        }
    </style>
    <!-- ================== END BASE CSS STYLE ================== -->

    @yield('style')

  </head>

  @if(!Auth::check())
    <body class="pace-top bg-white">
  @else
    <body>
  @endif

    @if(!Auth::check())

      <div id="page-container" class="fade">

    @else

      <div id="page-loader" class="fade in hidden"><span class="spinner"></span></div>
      <div id="page-container" class="fade page-sidebar-fixed page-header-fixed">

    @endif

    @if(Auth::check())
        @include('admin::base.navbar')
        @include('admin::base.menu')
    @endif

    @yield('conteudo')


    </div>
  <!-- end page container -->

    <!-- ================== BEGIN BASE JS ================== -->
    <script src="{{ URL::asset('/assets/js/core-'.env('ASSETS_VERSION').'.min.js')}}"></script>
    <script src="{{ URL::asset('/assets/js/form-'.env('ASSETS_VERSION').'.min.js')}}"></script>
    <script src="{{ URL::asset('/assets/js/app-'.env('ASSETS_VERSION').'.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/js/form-'.env('ASSETS_VERSION').'.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/js/liveeditor-form-'.env('ASSETS_VERSION').'.min.js') }}"></script>
    <!-- ================== END BASE JS ================== -->

  <script>
      $(document).ready(function() {
          $(".wysihtml5").wysihtml5()
          App.init();
          FormPlugins.init();

          @if ($errors->any())
            @foreach ($errors->all() as $error)
              $.gritter.add({
                title: 'Mesnsagem do sistema',
                text: '{{$error}}'
              });
            @endforeach
          @endif


      });

    
  </script>

   @yield('script')

</body>
</html>
