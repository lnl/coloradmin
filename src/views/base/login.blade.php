@extends('admin::base.container')

@section('conteudo')
    <div class="login bg-black animated fadeInDown">

        <div class="login-header">
            <div class="brand">
                <span class="logo"></span> {{env('APP_NAME')}}
                <small>silvermist CMS as a service</small>
            </div>
            <div class="icon">
                <i class="fa fa-sign-in"></i>
            </div>
        </div>

        <div class="login-content">

            @if(isset($msg) && !empty($msg)){{$msg}} @endif

            {{ Form::open(['url' => '/usuario/credencial', 'method' => 'POST', 'class' => "margin-bottom-0"]) }}
                <div class="form-group m-b-15">
                    <input type="text" class="form-control input-lg" name="email" placeholder="Email" />
                </div>

                <div class="form-group m-b-15">
                    <input type="password" class="form-control input-lg" name="senha" placeholder="Senha" />
                </div>

                <div class="login-buttons">
                    <button type="submit" class="btn btn-success btn-block btn-lg">Entrar</button>
                </div>

                <br/>

                @if(isset($fbLoginUrl) && !empty($fbLoginUrl))
                <div class="login-buttons">
                    <a href="{{$fbLoginUrl}}" class="btn btn-primary btn-block btn-lg"><i class="fa fa-facebook"></i></a>
                </div>
                @endif

                {{ Form::close() }}

                 <hr />

                <p class="text-center">
                    &copy; Lock n' Load All Right Reserved 2016
                </p>

        </div>

    </div>
@stop
