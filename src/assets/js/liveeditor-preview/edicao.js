$(".linha-editar .editControlers a.link-linha").click(function(e){
	e.preventDefault();
	var obj = $(e.target).parents(".linha-editar");

	parent.liveEditable("linha", obj);
});

$(".linha-editar .editControlers a.link-delete-linha").click(function(e){
	e.preventDefault();
	var obj = $(e.target).parents(".linha-editar");

	parent.remove("linha", obj);
});

$(".modulo-editar .editControlers a.link").click(function(e){
	e.preventDefault();
    var obj = $(e.target).parents(".modulo-editar")

	parent.liveEditable("modulo", obj);
});

$(".conteudo-editar .editControlers a.link").click(function(e){
	e.preventDefault();
    var obj = $(e.target).parents(".modulo-editar")

	parent.liveEditable("modulo", obj);
});
